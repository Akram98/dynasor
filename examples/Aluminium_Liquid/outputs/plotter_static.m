%
% Plotting script for static quantites from dynasor output file.
%

clear all
close all
clc

%Colors
red=[0.9 0.3 0.3];
blue=[0.3 0.3 0.9];
black=[0.1 0.1 0.1];
green=[0.3 0.9 0.3];
purple=[0.8 0.4 0.8];
%Colors

fontsize=20;
fontstyle='Times';


%Load dynasor output file
dynasor_outT1400_static_old


%%
LW=5.0;
figure('Position',[100 100,1500,650],'Color','w')

subplot(1,2,1)
plot(r,G_r_t_0_0(1,:),'Color',red,'LineWidth',LW)
xlabel('r [nm]')
ylabel('G(r,0)')

subplot(1,2,2)
plot(k(2:end),(F_k_t_0_0(1,2:end)),'Color',red,'LineWidth',LW);
xlabel('k [nm^{-1}]')
ylabel('S(k) = F(k,0)')
xlim([0 150])

set(findall(gcf,'-property','FontSize'),'FontSize',fontsize)
set(findall(gcf,'-property','FontName'),'FontName',fontstyle)

