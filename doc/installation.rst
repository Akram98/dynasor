.. index:: Installation

Installation
************

Installation via ``pip`` or ``conda``
=====================================

Stable versions of :program:`dynasor` are provided via `PyPI <https://pypi.org/project/dynasor/>`_ and as part of `conda-forge <https://anaconda.org/conda-forge/dynasor/>`_.
This implies that :program:`dynasor` can be installed using ``pip`` via::

    pip install dynasor

or using ``conda`` via::

    conda install -c conda-forge dynasor

:program:`dynasor` requires :program:`Python` 3.6 or higher,
:program:`numpy`, as well as a C99-complient C-compiler. 

Installation via ``setup.py``
=============================
.. index:: Git repository

If installation via ``pip`` fails or if you want to use the most recent
(development) version you can clone the repository and install using the
``setup.py`` script as follows::
	   
   git clone git@gitlab.com:materials-modeling/dynasor.git

or in the form of a tarball or zip-archive. On nix
systems, compilation is usually staightforward after cloning the repository 
and merely requires executing the following commands on the command line::

    cd dynasor
    python3 setup.py install --prefix=<build-path>

In case the compilation fails, one should check the settings in the
file ``build_config.py``.
   
After a successful compilation the binary can be found in
``<build-path>/bin`` whereas the libraries are located in
``<build-path>/lib``. One now ought to update the ``PATH`` environment
variable::

   export PATH=<build-path>/bin:$PATH

as well as the ``PYTHONPATH`` variable::
     
   export PYTHONPATH=<build-path>/lib/pythonX.Y/site-packages:$PYTHONPATH

where ``X.Y`` denotes the :program:`Python` version. If
:program:`dynasor` is used repeatedly it is a good idea to add these
commands to the ``.bashrc`` file (or equivalent).



