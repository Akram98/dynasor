.. _introduction:

.. index:: Introdution

Introduction
************

:program:`dynasor` is a tool for calculating total and partial
dynamic structure factors as well as related :ref:`correlation
functions <theory>` from molecular dynamics (MD) simulations. As main
input the program expects a trajectory (typically) from a MD
simulation, i.e., a file that contains as series of snapshots of
particle coordinates and optionally velocities for consecutive,
equally spaced slices in time.

The behavior of :program:`dynasor` is controlled via :ref:`command
line options <interface_cmdline>` that are provided at command
invocation. :program:`dynasor` also provides a
:ref:`Python interface <interface_python>` that can be useful for further analysis.

The most time-consuming part of the processing consists of the
calculation of the Fourier transformed densities. This part is implemented in C, and is
significantly accelerated by parallelization using OpenMP or OpenACC.
The time correlation calculation is performed explicitly in
:program:`Python` (using :program:`numpy` arrays), and is parallelized
with :program:`Python` threads (which works rather well thanks to
numpy releasing the :program:`Python` GIL). More comments concerning
the implementation can be found :ref:`here <implementation>`.

