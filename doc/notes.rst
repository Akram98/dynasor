.. index:: Limitations

Limitations
************
Sometimes problems can occur due to known FFT features such as
folding and aliasing. We've found the `filon` integration to work well for most
applications but sometimes it might be necessary to apply FFT windows to get a
better signal in the frequency domain.

The concept of :math:`\boldsymbol{q}`-points becomes muddy if the volume of the box is changing (which is the case, e.g., for NPT MD simulations).
Differences in positions between two frames with different boxes is also hard
to define. Therefore only constant volume MD trajectories are supported in
dynasor.
